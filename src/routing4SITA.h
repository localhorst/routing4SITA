#ifndef ROUTING4SITA_H
#define ROUTING4SITA_H

#define MAX_CHILDREN 3
#define MAX_DEPTH 4

typedef struct node NODE_t;
typedef struct threadData THREAD_DATA_t;
typedef struct bestPath BESTPATH_t;


extern NODE_t* getStartNode();

struct node {
    int id; //identifier for this node
    int nodescount; //how many children nodes
    NODE_t* nodesnext[MAX_CHILDREN]; //array of children nodes
    int nodescost[MAX_CHILDREN]; //array of cost per children node
};


struct threadData {
    int depth; //depth of the next node, e.g. start --> 0
    NODE_t* node; //next node to go
    int cost; //cost for the next node
    int predecessors[MAX_DEPTH]; // array with all predecessor
    BESTPATH_t* bestpath;
};


struct bestPath {
    int lowestCost;
    int predecessorsBestPath[MAX_DEPTH]; // array with all predecessor of the best path
    int depth; //depth of the best path, aka. how many nodes
};

#endif /* ROUTING4SITA_H */
